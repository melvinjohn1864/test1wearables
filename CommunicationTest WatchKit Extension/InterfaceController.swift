//
//  InterfaceController.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    var timer = Timer()
    
    var hunger = 0
    
    var health = 100
    
    var newHealth: Int!
    
    var name: String!
    
    var state: String!
    
    var hungerLevel: Int!
    
    var healthLevel: Int!
    
    var type: String!
    
    var image: String!

    
    // MARK: Outlets
    // ---------------------
    @IBOutlet var messageLabel: WKInterfaceLabel!
   
    // Imageview for the pokemon
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    // Label for Pokemon name (Albert is hungry)
    @IBOutlet var nameLabel: WKInterfaceLabel!
    // Label for other messages (HP:100, Hunger:0)
    @IBOutlet var outputLabel: WKInterfaceLabel!
    
    
    
    // MARK: Delegate functions
    // ---------------------

    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    // 3. Get messages from PHONE
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("WATCH: Got message from Phone")
        // Message from phone comes in this format: ["course":"MADT"]
        let messageBody = message["wake"] as! String
        messageLabel.setText(messageBody)
    
    }
    


    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        
        // 1. Check if teh watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
        if let val: [String: String] = context as? [String: String] {
            name = val["Name"]!
            image = val["imageName"]!
            
            self.nameLabel.setText("My name is " + name)
            self.pokemonImageView.setImage(UIImage(named: image))
            
        } else {
            self.nameLabel.setText("")
            
        }
//        let defaults = UserDefaults.standard
//
//        let pokemonName:String = defaults.string(forKey: "PokemonName")!
//
//
//        let healthLevel:String = defaults.string(forKey: "HealthLevel")!
//
//        let hungerLevel:String = defaults.string(forKey: "HungryLevel")!
        
        
//        self.outputLabel.setText("HP: \(healthLevel )  Hunger: \(hungerLevel)")
//        self.nameLabel.setText(pokemonName)
        

        
        
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    // MARK: Actions
    // ---------------------
    
    // 2. When person presses button on watch, send a message to the phone
    @IBAction func buttonPressed() {
        
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            self.messageLabel.setText("Sending msg to watch")
            WCSession.default.sendMessage(
                ["name" : "Pritesh"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
                    self.messageLabel.setText("Got reply from phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
                self.messageLabel.setText("Error sending message")
            })
        }
        else {
            print("Phone is not reachable")
            self.messageLabel.setText("Cannot reach phone")
        }
    }
    
    
    // MARK: Functions for Pokemon Parenting
    @IBAction func nameButtonPressed() {
        print("name button pressed")
    }

    @IBAction func startButtonPressed() {
        print("Start button pressed")
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: Selector("increaseTimer"), userInfo: nil, repeats: true)

        
    }
    
    @objc func increaseTimer(){
        
        hunger = hunger + 10
        
        newHealth = health
        
        if hunger >= 80 {
            health = health - 5
        }else {
            
        }
        
        if(health == 0){
            state = "dead"
            self.nameLabel.setText(name + " is dead")
            timer.invalidate()
        }else {
            state = "alive"
        }
        
        self.nameLabel.setText(name + " is hungry")
        self.outputLabel.setText("HP: \(newHealth ?? 100)  Hunger: \(hunger)")
        
        
        
    }
    
    @IBAction func feedButtonPressed() {
        print("Feed button pressed")
        
        hunger = hunger - 12
    }
    
    @IBAction func hibernateButtonPressed() {
        print("Hibernate button pressed")
        
        timer.invalidate()
        
        hungerLevel = hunger
        healthLevel = newHealth
        
        
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            self.messageLabel.setText("Sending hibernate msg to phone")
            WCSession.default.sendMessage(
                ["state" : state,"hungerLevel" : hungerLevel, "healthLevel" : healthLevel, "name" : name, "type" : image],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
                    self.messageLabel.setText("Got reply from phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
                self.messageLabel.setText("Error sending message")
            })
        }
        else {
            print("Phone is not reachable")
            self.messageLabel.setText("Cannot reach phone")
        }
        
        
    }
    
}
